package com.example.anh.cookingrecipe;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by segaw on 5/1/2017.
 */

public class AdapterSavedRecipe extends BaseAdapter{
    ArrayList<ItemSaved> dsMonAnDaLuu;
    LayoutInflater inflater;
    Context context;

    public AdapterSavedRecipe(Context context, ArrayList<ItemSaved> dsMonAnDaLuu) {
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.dsMonAnDaLuu = dsMonAnDaLuu;
        this.context = context;
    }

    @Override
    public int getCount() {
        return dsMonAnDaLuu.size();
    }

    @Override
    public Object getItem(int position) {
        return dsMonAnDaLuu.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
// Lấy ra đối tượng cần hiển thị ở vị trí thứ position
        final ItemSaved item = dsMonAnDaLuu.get(position);
// Khai báo các component
        final TextView txtId, txtTen, txtNlieu, txtTLoai, txtCLam, tvChiTiet;
        ImageView imaHinh;
// Khởi tạo view.
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_mon_an_luu, parent, false);
        }

        txtId = (TextView) convertView.findViewById(R.id.iIDLuu);
        txtTen = (TextView) convertView.findViewById(R.id.iTenLuu);
        txtNlieu = (TextView) convertView.findViewById(R.id.iNlieuLuu);
        txtTLoai = (TextView) convertView.findViewById(R.id.iTheLoaiLuu);
        txtCLam = (TextView) convertView.findViewById(R.id.iCLamLuu);
        tvChiTiet = (TextView) convertView.findViewById(R.id.iChiTietLuu);

        imaHinh = (ImageView) convertView.findViewById(R.id.iHinhLuu);
// Set dữ liệu vào item của list view
        txtId.setText(item.getId() + "");
        txtTen.setText(item.getTen());
        txtTLoai.setText(item.getTheloai());
        txtNlieu.setText(item.getNguyenLieu());
        txtCLam.setText(item.getCachLam());
        if (!TextUtils.isEmpty(item.getHinh())) {
            Picasso.with(context).load(item.getHinh()).resize(700,400).into(imaHinh);
        }

        tvChiTiet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chiTiet = new Intent(context, ChiTietMonAn.class);
                chiTiet.putExtra("id",txtId.getText().toString());
                chiTiet.putExtra("ten",item.getTen());
                chiTiet.putExtra("theloai",item.getTheloai());
                chiTiet.putExtra("nlieu",item.getNguyenLieu());
                chiTiet.putExtra("clam",item.getCachLam());
                chiTiet.putExtra("hinh",item.getHinh());
                chiTiet.putExtra("trangthailuu","yes");
                context.startActivity(chiTiet);
            }
        });

        return convertView;
    }
}
