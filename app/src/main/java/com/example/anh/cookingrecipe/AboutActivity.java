package com.example.anh.cookingrecipe;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.AdapterView;
import java.util.ArrayList;

/**
 * Created by segaw on 4/15/2017.
 */

public class AboutActivity extends AppCompatActivity {
    final String url = "http://nhutstudio.com";
    private ArrayList<ItemAbout> lstAbout(){
        ArrayList<ItemAbout> abouts = new ArrayList<ItemAbout>();
        abouts.add(new ItemAbout("Tên ứng dụng","Cooking Recipe"));
        abouts.add(new ItemAbout("Phiên bản","1.0"));
        abouts.add(new ItemAbout("Created By","Hot Team"));
        abouts.add(new ItemAbout("Số điện thoại","096 xxx xxxx"));
        abouts.add(new ItemAbout("Visit us"," "));
        return abouts;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ListView listView = (ListView)findViewById(R.id.lstAbout);
        AdapterAbout adapterAbout = new AdapterAbout(getApplicationContext(), lstAbout());
        listView.setAdapter(adapterAbout);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (String.valueOf(id).equals("0")){

                }
                else if (String.valueOf(id).equals("1")){

                }
                else if (String.valueOf(id).equals("2")){

                }
                else if (String.valueOf(id).equals("3")){

                }
                else if (String.valueOf(id).equals("4")){

                }
                else if (String.valueOf(id).equals("5")){
                    Intent web = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(web);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home)
        {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);

    }
}
