package com.example.anh.cookingrecipe;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by segaw on 4/16/2017.
 */

public class AdapterRecipe extends ArrayAdapter<ItemRecipe> {
    TextView txtTen, txtCachLam, txtNguyenLieu, txtLuu, txtTheLoai, txtChiTiet, txtID;
    ImageView hinh;
    ArrayList<ItemRecipe> recipes;
    Context context;
    SQLiteMonAn sqLiteMonAn;
    public AdapterRecipe(Context context, int resource, ArrayList<ItemRecipe> arrayList) {
        super(context, R.layout.item_monan, arrayList);
        this.context = context;
        this.recipes = arrayList;
    }



    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.item_monan, null);
        }
        final ItemRecipe itemRecipe = recipes.get(position);
        if (itemRecipe != null) {
            txtTen = (TextView)view.findViewById(R.id.iTen);
            txtTheLoai = (TextView)view.findViewById(R.id.iTheLoai);
            txtNguyenLieu = (TextView)view.findViewById(R.id.iNlieu);
            txtCachLam = (TextView)view.findViewById(R.id.iCLam);
            txtChiTiet = (TextView)view.findViewById(R.id.iChiTiet);
            txtLuu = (TextView)view.findViewById(R.id.iLuu);
            hinh = (ImageView)view.findViewById(R.id.iHinh);



            txtTen.setText(itemRecipe.getTen());
            txtTheLoai.setText(itemRecipe.getTheloai());
            txtNguyenLieu.setText(itemRecipe.getNguyenLieu());
            txtCachLam.setText(itemRecipe.getCachLam());
            if(!TextUtils.isEmpty(itemRecipe.getHinh())) {
                Picasso.with(getContext()).load(itemRecipe.getHinh()).resize(700, 400).into(hinh);
            }

        }

        txtChiTiet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chiTiet = new Intent(getContext(), ChiTietMonAn.class);
                int id = itemRecipe.getId();
                chiTiet.putExtra("id",String.valueOf(id));
                chiTiet.putExtra("ten",itemRecipe.getTen());
                chiTiet.putExtra("theloai",itemRecipe.getTheloai());
                chiTiet.putExtra("nlieu",itemRecipe.getNguyenLieu());
                chiTiet.putExtra("clam",itemRecipe.getCachLam());
                chiTiet.putExtra("hinh",itemRecipe.getHinh());
                chiTiet.putExtra("trangthailuu","no");
                getContext().startActivity(chiTiet);
            }
        });

        txtLuu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = itemRecipe.getId();
                String ten = itemRecipe.getTen();
                String tl = itemRecipe.getTheloai();
                String nl = itemRecipe.getNguyenLieu();
                String cl = itemRecipe.getCachLam();
                String hinh = itemRecipe.getHinh();
                sqLiteMonAn = new SQLiteMonAn(getContext());
                if (sqLiteMonAn.LuuMonAn(String.valueOf(id), ten, tl, nl, cl, hinh)){
                    Toast.makeText(getContext(), "Lưu thành công!", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getContext(), "Món ăn này đã được lưu trước đó!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }

}
