package com.example.anh.cookingrecipe;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ListView;

import com.nhutstudio.monngon365.R;
import com.nhutstudio.monngon365.adapter.AdapterLuu;
import com.nhutstudio.monngon365.module.MonAnDaLuu;
import com.nhutstudio.monngon365.other.SQLiteMonAn;

import java.util.ArrayList;

public class LuuActivity extends AppCompatActivity {
    ListView lts;
    SQLiteMonAn sqLiteMonAn;
    ArrayList<ItemSaved> list;
    AdapterSavedRecipe adapterLuu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_luu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //Hiển thị các món ăn đã lưu lên listview
        lts = (ListView)findViewById(R.id.lstMonAnDaLuu);
        sqLiteMonAn = new SQLiteMonAn(LuuActivity.this);
        list = new ArrayList<ItemSaved>();
        list = sqLiteMonAn.DanhSachMonAn();
        adapterLuu = new AdapterSavedRecipe(LuuActivity.this, list);
        if (list!=null){
            adapterLuu = new AdapterSavedRecipe(this, list);
            lts.setAdapter(adapterLuu);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
