package com.example.anh.cookingrecipe;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.Spanned;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

/**
 * Created by Anh on 4/25/2017.
 */
public class ChiTietMonAn extends AppCompatActivity {
    TextView tvTen, tvNL, tvCL, tvID, tvLuu;
    ImageView img;
    LinearLayout lnLuu;
    SQLiteMonAn sqLiteMonAn;
    String idMonAn;
    String trangthai;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chi_tiet_mon_an);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Nhận dữ liệu được gữi từ AdapterLuu và HomeFragment
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }
        idMonAn = extras.getString("id");
        final String t = extras.getString("ten");
        final String tloai = extras.getString("theloai");
        final String nl = extras.getString("nlieu");
        final String cl = extras.getString("clam");
        final String hinh = extras.getString("hinh");
        trangthai = extras.getString("trangthailuu");

        tvID = (TextView) findViewById(R.id.idChiTiet);
        lnLuu = (LinearLayout) findViewById(R.id.lnLuu);
        tvNL = (TextView) findViewById(R.id.nguyenLieu);
        tvTen = (TextView) findViewById(R.id.tvTen);
        tvCL = (TextView) findViewById(R.id.cachLam);
        tvLuu = (TextView) findViewById(R.id.tvLuuMain);
        img = (ImageView) findViewById(R.id.imgAnhMonAn);
        //Nếu đã lưu thì ẩn nút lưu
        if (trangthai.equals("yes")) {
            tvLuu.setVisibility(View.GONE);
            lnLuu.setVisibility(View.GONE);
        } else {

        }

        //Hiển thị nguyên liệu và cách làm dạng html
        Spanned ngLieu = Html.fromHtml(nl);
        Spanned caLam = Html.fromHtml(cl);

        sqLiteMonAn = new SQLiteMonAn(ChiTietMonAn.this);
        tvID.setText(idMonAn);
        tvNL.setText(ngLieu);
        tvCL.setText(caLam);
        tvTen.setText(t);


        final String idMA = tvID.getText().toString();
        tvLuu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //nếu chưa lưu thì tiến hành lưu vào SQLite
                //lưu rồi thì báo là lưu rồi
                if (sqLiteMonAn.LuuMonAn(idMA.toString(), t, tloai, nl, cl, hinh)) {
                    Toast.makeText(getApplicationContext(), "Lưu thành công!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Món ăn này đã được lưu trước đó!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Picasso.with(ChiTietMonAn.this)
                //Link ảnh cần load
                .load(hinh)
                // optional
                .placeholder(R.mipmap.ic_launcher)
                //Nếu không load được
                .error(R.mipmap.ic_launcher)
                //Set kích thước ảnh
                .resize(700, 400)
                //Gán ảnh lên ImageView
                .into(img);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Khởi tạo icon xóa trên action bar
        if (trangthai.equals("yes")) {
            getMenuInflater().inflate(R.menu.delete_icon, menu);
        } else {


        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //Quay lại MainActivity
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
            //Xóa Món ăn đã lưu
        } else if (id == R.id.action_delete_icon) {
            AlertDialog.Builder thongBao = new AlertDialog.Builder(ChiTietMonAn.this);
            thongBao.setTitle("Xóa");
            thongBao.setMessage("Bạn có chắc là muốn xóa món ăn này hay không?");
            thongBao.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    sqLiteMonAn.XoaMonAnDaLuu(tvID.getText().toString());
                    finish();
                    Toast.makeText(getApplicationContext(), "Đã xóa", Toast.LENGTH_LONG).show();
                }
            });
            thongBao.setNegativeButton("Quay lại", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // do nothing
                }
            });
            thongBao.setIcon(android.R.drawable.ic_dialog_alert);
            AlertDialog xoa = thongBao.create();
            xoa.show();
            return true;
        }

        return super.onOptionsItemSelected(item);

    }
}
