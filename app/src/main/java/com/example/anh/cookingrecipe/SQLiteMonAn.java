package com.example.anh.cookingrecipe;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;


public class SQLiteMonAn extends SQLiteOpenHelper {

    private SQLiteDatabase db;

    public SQLiteMonAn(Context context){
        super(context, "monngon365", null, 1);
    }
    //Khoi tao co so du lieu
    static final String KEY_ID = "id";
    static final String KEY_TEN = "ten";
    static final String KEY_TLOAI = "theloai";
    static final String KEY_NLIEU = "nlieu";
    static final String KEY_CLAM = "clam";
    static final String KEY_HINH = "hinh";

    static final String DATABASE_NAME = "monngon365";
    static final String DATABASE_TABLE = "monandaluu";
    static final String DATABSE_CREATE_TABLE = "create table monandaluu(id integer primary key," + "ten text not null, theloai text not null, nlieu text not null, clam text not null, hinh text not null);";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABSE_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //bỏ bảng dữ liệu cũ
        db.execSQL("drop table if exists monandaluu "+ DATABASE_TABLE);
        onCreate(db);
    }

    public Boolean LuuMonAn(String id, String ten, String theloai, String nlieu, String clam, String hinh){
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_ID, id);
        contentValues.put(KEY_TEN, ten);
        contentValues.put(KEY_TLOAI, theloai);
        contentValues.put(KEY_NLIEU, nlieu);
        contentValues.put(KEY_CLAM, clam);
        contentValues.put(KEY_HINH, hinh);
        return this.getWritableDatabase().insert(DATABASE_TABLE, null, contentValues)>0;
    }
    public ArrayList<ItemSaved> DanhSachMonAn(){
        ArrayList<ItemSaved> list = new ArrayList<ItemSaved>();
        //mở để đọc dữ liệu
        Cursor cursor = this.getReadableDatabase().rawQuery("Select id, ten, theloai, nlieu, clam, hinh from monandaluu", null);
        //di chuyển về dòng đầu, nếu danh sách rỗng sẽ trả về false
        if (!cursor.moveToFirst())
            return null;
        do {
            ItemSaved ma = new ItemSaved();
            ma.id = cursor.getInt(0);
            ma.ten = cursor.getString(1);
            ma.theloai = cursor.getString(2);
            ma.nguyenLieu = cursor.getString(3);
            ma.nguyenLieu = cursor.getString(4);
            ma.hinh = cursor.getString(5);
            list.add(ma);
        }while (cursor.moveToNext());
        return list;
    }
    //Xóa món ăn đã lưu dựa vào id món ăn
    public boolean XoaMonAnDaLuu (String id){
        return this.getWritableDatabase().delete(DATABASE_TABLE, KEY_ID + "=" + id, null)>0;
    }
    



}
