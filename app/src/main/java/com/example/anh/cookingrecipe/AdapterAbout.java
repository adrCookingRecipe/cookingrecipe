package com.example.anh.cookingrecipe;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.anh.cookingrecipe.ItemAbout;

import java.util.ArrayList;

/**
 * Created by segaw on 4/15/2017.
 */

public class AdapterAbout extends ArrayAdapter {
    Context context;
    ArrayList<ItemAbout> lstAbout;

    public AdapterAbout(Context context, ArrayList<ItemAbout> lstAbout){
        super(context,R.layout.item_about, lstAbout);
        this.lstAbout = lstAbout;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        View rowView = null;
        rowView = inflater.inflate(R.layout.item_about, parent, false);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.tieuDe);
        txtTitle.setText(lstAbout.get(position).getTitle());

        TextView txtContent = (TextView) rowView.findViewById(R.id.noiDung);
        txtContent.setText(lstAbout.get(position).getContent());

        return  rowView;
    }
}
