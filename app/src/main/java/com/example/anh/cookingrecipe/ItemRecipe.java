package com.example.anh.cookingrecipe;

/**
 * Created by segaw on 4/16/2017.
 */

public class ItemRecipe {
    public int id;
    public String ten;
    public String theloai;
    public String nguyenLieu;
    public String cachLam;
    public String hinh;

    public ItemRecipe(int id, String ten, String theloai, String nguyenLieu, String cachLam, String hinh) {
        this.id = id;
        this.ten = ten;
        this.theloai = theloai;
        this.nguyenLieu = nguyenLieu;
        this.cachLam = cachLam;
        this.hinh = hinh;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getTheloai() {
        return theloai;
    }

    public void setTheloai(String theloai) {
        this.theloai = theloai;
    }

    public String getNguyenLieu() {
        return nguyenLieu;
    }

    public void setNguyenLieu(String nguyenLieu) {
        this.nguyenLieu = nguyenLieu;
    }

    public String getCachLam() {
        return cachLam;
    }

    public void setCachLam(String cachLam) {
        this.cachLam = cachLam;
    }

    public String getHinh() {
        return hinh;
    }

    public void setHinh(String hinh) {
        this.hinh = hinh;
    }
}
