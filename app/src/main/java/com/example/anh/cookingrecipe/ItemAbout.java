package com.example.anh.cookingrecipe;

/**
 * Created by segaw on 4/15/2017.
 */

public class ItemAbout {
    private String title;
    private String content;

    public ItemAbout(String title, String content){
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


}
