package com.example.anh.cookingrecipe;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {

    String url;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    ArrayList<ItemRecipe> monAn = new ArrayList<ItemRecipe>();
    ListView lst;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            url = bundle.getString("url");
        }
        new JSMonNuong().execute();
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        lst = (ListView)view.findViewById(R.id.lstMonAn);
        // Inflate the layout for this fragment

        return view;

    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }





    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
    class JSMonNuong extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            return DocURL(url);
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                JSONArray lstMonAn = new JSONArray(s);
                for (int i = 0; i < lstMonAn.length(); i++) {
                    final JSONObject ma = lstMonAn.getJSONObject(i);

                    monAn.add(new ItemRecipe(ma.getInt("id"), ma.getString("ten"), ma.getString("theloai"), ma.getString("nlieu"), ma.getString("clam"), ma.getString("hinh")));
                    final AdapterRecipe adapterMonAn = new AdapterRecipe(getActivity(), R.layout.item_monan, monAn);
                    lst.setAdapter(adapterMonAn);

                    lst.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            String ten = adapterMonAn.getItem(i).getTen().toString();
                            String id = String.valueOf(adapterMonAn.getItem(i).getId());
                            String theloai = adapterMonAn.getItem(i).getTheloai().toString();
                            String nlieu = adapterMonAn.getItem(i).getNguyenLieu().toString();
                            String clam = adapterMonAn.getItem(i).getCachLam().toString();
                            String hinh = adapterMonAn.getItem(i).getHinh().toString();

                            Intent chiTiet = new Intent(getContext(), ChiTietMonAn.class);
                            chiTiet.putExtra("id",id);
                            chiTiet.putExtra("ten",ten);
                            chiTiet.putExtra("theloai",theloai);
                            chiTiet.putExtra("nlieu",nlieu);
                            chiTiet.putExtra("clam",clam);
                            chiTiet.putExtra("hinh",hinh);
                            chiTiet.putExtra("trangthailuu","no");
                            getContext().startActivity(chiTiet);
                        }
                    });
                    lst.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                        @Override
                        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                            return false;
                        }
                    });
                    super.onPostExecute(s);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
    private static String DocURL (String theUrl) {
        StringBuilder content = new StringBuilder();

        try {
            URL url = new URL(theUrl);

            URLConnection urlConnection =  url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
//            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is, "utf-8"),8);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                content.append(line + "\n");
            }
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        return content.toString().substring(content.indexOf("{"), content.lastIndexOf("}") + 1);
        return content.toString();
    }
}

